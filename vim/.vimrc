set tabstop=4
set shiftwidth=4
set expandtab
set autoindent

set nohlsearch

set clipboard=unnamed
set number
set encoding=utf-8
set mouse=""
set backspace=indent,eol,start

syntax on
colorscheme desert
